﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using AstronautGame.Classes;

//Editor:       Schneider Jan
//Erstellt:     01.04.2021
//Bearbeitet:   10.04.2021
//Status:       In Bearbeitung
public class Health : MonoBehaviour
{
    public float maxLifePoints;     //Maximale Leben (Erzeugt nach der Anzahl die Herzen)
    public float lifePoints;        //Aktuelle Leben
   
    public Transform parentTransform;   //Positions Game Object im Canvas an der Position an welcher die Herzen erzeugt werden sollen
    public Sprite lifeImage;            //Bild der Herzen
    public int shiftHorizontal;         //Versatz Horizontale Richtung
    public string nameOfObject;         //Name der Herzen    
    public float sizeOfImage;           //Größe der Bilder

    public float damageDelay;           //Verzögerung zwischen dem Schaden in Sekunden


    bool isDamageable = true;
    int counter;

    void Start()
    {
        /*
        GameObjectCreator Creator = new GameObjectCreator();
        List<GameObject> Hearts =  Creator.Create(maxLife, nameOfObject);
        GameObjectGroup heartGroup = new GameObjectGroup("Herzen", parentPosition.transform, Hearts);
        heartGroup.ChangeParent();
        heartGroup.AddComponents<Image>();       
        */
        GetLife(0);               //Leben Generieren
        /*
        for (int i = 0; i < maxLifePoints; i++)
        {
            /*
            GameObject NewObj = new GameObject();                           //Neues Object erzeugen
            NewObj.name = nameOfObject + i.ToString();                      //Namen vergeben
            NewObj.AddComponent<Image>();                                   //Komponente Image hinzufügen
            NewObj.GetComponent<Image>().sprite = lifeImage;                //Herz Image einfügen
            
            NewObj.transform.parent = parentPosition.transform;             //In die Gruppe des Parent Object schieben
            NewObj.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(sizeOfImage, sizeOfImage); //Größe der Bilder Festlegen

            Vector3 myPosition = parentPosition.transform.position;         //Aktuelle Position des Parent object in einer Vector3 Variable speichern
            myPosition.x += shiftHorizontal * i;                            //Verschiebung draufaddieren
            NewObj.transform.position = myPosition;                         //Gespeicherte & Geänderte Vector3 Variable an das Erzeugte Object übergeben
            
            GetLife(i);
        }
    */
        
    }

    void FixedUpdate()
    {  
        if (!isDamageable)
        {
            counter += 1;
            if (counter >= (1/ Time.fixedDeltaTime)* damageDelay)
            {
                ResetIsDamageable();
                counter = 0;
            }
        }
        else
            counter = 0;

        

    }


    public void GetDamage(float damage)
    {
        if (isDamageable)
        {
            lifePoints -= damage;
            lifePoints = Mathf.Max(0, lifePoints);
            isDamageable = false;
            Destroy(GameObject.Find(nameOfObject + lifePoints));
        }           
    }

    void ResetIsDamageable()
    {
        isDamageable = true;
    }

    void GetLife(float life)
    {
        lifePoints += life;                                                 //Lebenspunke auffüllen

        if (lifePoints > maxLifePoints)                                     //Wenn Leben über Maximale leben, dann Leben gleich Maximale Leben.
            lifePoints = maxLifePoints;

        for (int i = 0; i < lifePoints; i++)
        {
            Destroy(GameObject.Find(nameOfObject + i));                     //Falls es objekt schon gibt wird es erst Zerstört um dann neu aufgebaut zu werden
            GameObject NewObj = new GameObject();                           //Neues Object erzeugen
            NewObj.name = nameOfObject + i.ToString();                      //Namen vergeben
            NewObj.AddComponent<Image>();                                   //Komponente Image hinzufügen
            NewObj.GetComponent<Image>().sprite = lifeImage;                //Herz Image einfügen

            NewObj.transform.SetParent(parentTransform);
            //NewObj.transform.parent = parentPosition.transform;             //In die Gruppe des Parent Object schieben
            NewObj.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(sizeOfImage, sizeOfImage); //Größe der Bilder Festlegen

            Vector3 myPosition = parentTransform.position;         //Aktuelle Position des Parent object in einer Vector3 Variable speichern
            myPosition.x += shiftHorizontal * i;                            //Verschiebung draufaddieren
            NewObj.transform.position = myPosition;                         //Gespeicherte & Geänderte Vector3 Variable an das Erzeugte Object übergeben
        }
        
    }
}
