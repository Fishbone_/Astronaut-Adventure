using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AstronautGame.Classes;


//Editor:       Wilkin Niclas
//Erstellt:     22.04.2021
//Bearbeitet:   22.04.2021
//Status:       In Bearbeitung

public class PlayerController : MonoBehaviour
{
    public float MaxSpeed = 4;
    public float JumpPower = 550;
    public float LaserSpeed = 500;

    public LayerMask GroundLayer;
    public Transform GroundCollider;
    public GameObject LaserPrefab;
    public Transform Spawnpoint;

    [HideInInspector]
    public bool IsLookingRight
    {
        get
        {
            if (transform.localScale.x > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
 
    private Rigidbody2D rb2d;
    private Animator anim;
    private AttackHandler playerAttackHandler;
    private JumpHandler playerJumpHandler;
    private MoveHandler playerMoveHandler;

 

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        playerAttackHandler = new AttackHandler();
        playerAttackHandler.PlayerIsAttacking += OnPlayerAttacking; //Subcribe Method to Event PlayerIsAttacking
        playerJumpHandler =  new JumpHandler(rb2d, GroundCollider, GroundLayer);
        playerMoveHandler = new MoveHandler(rb2d)
        {
            LookingRight = IsLookingRight
        };
        playerMoveHandler.DirectionChanged += OnDirectionChanged;//Subcribe Method to Event DirectionChanged
    }

    // Update is called once per frame
    void Update()
    {
        playerJumpHandler.CatchJumpCommand(); //Abfragen ob Jump Command ausgel�st wird
        playerAttackHandler.CatchAttackCommand(); //Abfragen ob Attack Command ausgel�st wird
    }

    private void FixedUpdate()
    {
        

        #region Player Movement
        playerMoveHandler.CheckPlayerDirection();
        playerMoveHandler.SetVelocity(MaxSpeed);
        #endregion

        #region Player Animation
        anim.SetFloat("SpeedRunning", Mathf.Abs(playerMoveHandler.Hor));
        anim.SetBool("IsGrounded", playerJumpHandler.HasGroundConnection);
        #endregion

        #region Player Jump
        playerJumpHandler.CheckGroundConnection(); //Pr�fen Verbindung zum Boden

        if (playerJumpHandler.HasActiveJump)
        {
            playerJumpHandler.ExecuteJump(JumpPower);
        }
        #endregion

        #region Player Shoot
        playerAttackHandler.ExecuteAttackWhenCommandCatched();
        #endregion

    }

    public void OnDirectionChanged(object sender, EventArgs e)
    {
        playerMoveHandler.LookingRight = !playerMoveHandler.LookingRight; //Invertieren Blickrichtung
        Vector3 myscale = transform.localScale; //Abfrage Momentaner Scale des Players
        myscale.x *= -1; //Umdrehen des Scale auf der x-Achse(�nderung Richtung der Textures)
        transform.localScale = myscale; //Setzen der �nderungen auf den Transform des Players
    }

    public void OnPlayerAttacking(object sender, EventArgs e) 
    {
        anim.SetTrigger("Attack"); //Setzt Attack Animation
        GameObject laser = Instantiate(LaserPrefab, Spawnpoint.position, Quaternion.identity); //Clone von LaserPrefab erzeugen
        if (IsLookingRight)
        {
            laser.GetComponent<Rigidbody2D>().AddForce(Vector3.right * LaserSpeed); //Flugbahn festlegen Laser
        }
        else 
        {
            laser.GetComponent<Rigidbody2D>().AddForce(Vector3.left * LaserSpeed); //Flugbahn festlegen Laser
        }
        
    }
    
}
