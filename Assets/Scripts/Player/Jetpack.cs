﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//Editor:       Schneider Jan
//Erstellt:     30.03.2021
//Bearbeitet:   01.04.2021
//Status:       In Bearbeitung
public class Jetpack : MonoBehaviour
{
    public float fuelPercentage;        //Füllstand 
    public float jetpackStrenght;       //Stärke des Jetpacks
    public float fuelConsumption;       //Spritverbrauch Je Höher des so Sparsamer
    public Image fuelUI;                //Anzeige Füllstand Jetpack Fuel


    public GameObject flame;            //Flamme des Jetpack    
    public GameObject player;           //Spieler
    
    // Local Variables
    double flameStartX;                 //Lokaler Start Punkt X
    double flameStartY;                 //Lokaler Start Punkt Y
    double pushFlameX;                  //Bewegungspunkt X
    double pushFlameY;                  //Bewegungspunkt Y
    Rigidbody2D rb2d;                   //Rigidbody 2D vom Spieler
    Animator anim;                      //Animator vom Spieler

    // Start is called before the first frame update
    void Start()
    {
        flameStartX = flame.transform.localPosition.x;  //Startposition der Flamme
        flameStartY = flame.transform.localPosition.y;
        rb2d = player.GetComponent<Rigidbody2D>();      //Rigidbody 2D des Spielers auslesen
        anim = GetComponent<Animator>();                //Animator vom Spieler auslesen
    }
     
    void FixedUpdate()
    {

        float ver = Input.GetAxis("Vertical");          //

        pushFlameX = (ver * -0.09) + flameStartX;
        pushFlameY = (ver * -0.2) + flameStartY;
               
        if (fuelPercentage > 0)
        {
            if (ver > 0.001)
            {
                rb2d.velocity = new Vector2(0, ver * jetpackStrenght);
                flame.transform.localPosition = new Vector2((float)pushFlameX, (float)pushFlameY);
                anim.SetBool("IsFlying", true);
            }
            fuelPercentage = fuelPercentage - (ver/ fuelConsumption);
            if (ver == 0)
                anim.SetBool("IsFlying", false);
        }
        else
        {
            fuelPercentage = 0;
            flame.transform.localPosition = new Vector2((float)flameStartX, (float)flameStartY);
        }

        fuelUI.fillAmount = fuelPercentage / 100;
    }

    void RefillFuel(float Refill)
    {
        fuelPercentage = fuelPercentage + Refill;
    }
    
}

