using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Editor:       Schneider Jan
//Erstellt:     12.04.2021
//Bearbeitet:   12.04.2021
//Status:       In Bearbeitung
public class LifePoints : MonoBehaviour
{
    public float refillLife = 1;            //Anzahl wie viel Leben aufgefüllt werden

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.SendMessage("GetLife", refillLife);
            Destroy(gameObject);
        }

    }
}

