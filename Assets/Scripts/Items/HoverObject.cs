using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Editor:       Schneider Jan
//Erstellt:     22.04.2021
//Bearbeitet:   22.04.2021
//Status:       In Bearbeitung
public class HoverObject : MonoBehaviour
{
    
    public float speedVertical;
    public float rangeVertical;

    Rigidbody2D rb2d;
    Transform trans;

     float startY;
     float actualY;


    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        startY = trans.position.y;
    }

    void FixedUpdate()
    {
        actualY = trans.position.y;

        if (actualY <= startY)
            rb2d.velocity = new Vector2(0, speedVertical);
        if (actualY >= (startY + rangeVertical))
            rb2d.velocity = new Vector2(0, -speedVertical);
    }

}
