﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Editor:       Schneider Jan
//Erstellt:     31.03.2021
//Bearbeitet:   31.03.2021
//Status:       In Bearbeitung
public class Fuel : MonoBehaviour
{
    public float refillLevel;
    
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("Player"))
        {
            other.SendMessage("RefillFuel",refillLevel);
            Destroy(gameObject);
        }

    }
}
