using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Editor:       Schneider Jan
//Erstellt:     22.04.2021
//Bearbeitet:  29.04.2021
//Bearbeitet durch: Wilkin Niclas 
//Status:       In Bearbeitung
public class EnemyMovement : MonoBehaviour
{
    #region Public Variables
    public float speedHorizontal = 4;
    public float HP = 25;
    #endregion

    bool lookingRight = true;
    Rigidbody2D rb2d;
    

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }


    void FixedUpdate()
    {
        rb2d.velocity = new Vector2(speedHorizontal, rb2d.velocity.y);
        if (HP <= 0)
        {
            GameObject.Destroy(gameObject);
        }
    }



    void OnTriggerEnter2D(Collider2D other)
    {        
        if (!other.CompareTag("Player") && !other.CompareTag("Bottom"))
        {
            Flip();
        }
    }

    private void GetDamage(float damage)
    {
            print($"{gameObject.name} wurde getroffen und hat {damage} Schaden erhalten!");
            HP -= damage;
    }


    void Flip()
    {
        Vector2 vec2 = transform.localScale;
        vec2.x *= -1;
        transform.localScale = vec2;
        lookingRight = !lookingRight;
        speedHorizontal *= -1;        
    }
}
