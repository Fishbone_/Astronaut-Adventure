using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Editor:       Schneider Jan
//Erstellt:     12.04.2021
//Bearbeitet:   29.04.2021
//Bearbeitet durch: Wilkin Niclas
//Status:       In Bearbeitung
public class Damage : MonoBehaviour
{
    public float damage = 1;            //Anzahl Schaden/Standardm??ig 1  
    public string damageToTag;          //Tag Welchem geschadet werden soll
    private bool makesDamage { get; set; } = false;
   
    void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.layer == LayerMask.NameToLayer("Enemy") &&  other.gameObject.layer != LayerMask.NameToLayer("GameWorld") && other.gameObject.layer != LayerMask.NameToLayer("Weapon"))
        {
             other.SendMessage("GetDamage", damage);
        }
        else if (gameObject.layer == LayerMask.NameToLayer("Enemy") && other.gameObject.layer == LayerMask.NameToLayer("GameWorld"))
        {
            
        }
        else if (gameObject.layer == LayerMask.NameToLayer("Weapon") && other.gameObject.layer != LayerMask.NameToLayer("GameWorld") && !makesDamage)
        {
            makesDamage = true;
            other.SendMessage("GetDamage", damage);  
        }
        else if (gameObject.layer == LayerMask.NameToLayer("Weapon") && other.gameObject.layer == LayerMask.NameToLayer("GameWorld"))
        {
            GameObject.Destroy(gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.layer == LayerMask.NameToLayer("Weapon"))
        {
            GameObject.Destroy(gameObject);
            makesDamage = false;
        }
    }
}
