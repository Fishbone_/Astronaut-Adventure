using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public GameObject menu;
    public bool menueOpened;

    private void Start()
    {
        menu.SetActive(false);
    }

    void Update()
    {
       if (Input.GetButtonDown("Cancel"))
        {            
            menueOpened = !menueOpened;
            Pause(menueOpened);
        }      

        //menu.SetActive(menueOpened);

    }

    public void Pause(bool pause)
    {
        if (pause)
            Time.timeScale = 0;
        else
        {
            Time.timeScale = 1;
            menueOpened = false;
        }

        menu.SetActive(pause);
    }


}
