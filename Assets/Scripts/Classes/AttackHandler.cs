using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Editor:       Wilkin Niclas
//Erstellt:     22.04.2021
//Bearbeitet:   22.04.2021
//Status:       In Bearbeitung
public class AttackHandler : PauseChecker
{
    public event PlayerDelegate PlayerIsAttacking;

    public bool HasActiveAttack { get; private set; }

    public AttackHandler()
    {
        HasActiveAttack = false;
    }

    public void CatchAttackCommand()
    {
        if (Input.GetButtonUp("Fire1") && !HasActiveAttack && !IsGamePaused)
        {
            HasActiveAttack = true;
        }
    }

    public void ExecuteAttackWhenCommandCatched()
    {
        if (HasActiveAttack)
        {
            
            PlayerIsAttacking(this, new EventArgs());
            HasActiveAttack = false;
        }
    }

}
