﻿using System;
using System.Collections.Generic;
using UnityEngine;

    public class PauseChecker
    {
        public bool IsGamePaused
        {
            get
            {
                if (Time.timeScale == 0) return true;
                else return false;
            }
        }
    }

