using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Editor:       Wilkin Niclas
//Erstellt:     22.04.2021
//Bearbeitet:   22.04.2021
//Status:       In Bearbeitung
public class JumpHandler : PauseChecker
{
    private readonly Rigidbody2D jumperRigidbody;
 
    public float OverlapRatio { get; set; }

    public bool HasActiveJump
    {
        get;
        private set;
    }
    public bool HasGroundConnection
    {
        get;
        private set;
    }


    public Transform GroundCollider;
    public LayerMask GroundLayer;

    public JumpHandler(Rigidbody2D rigidbody, Transform groundCollider, LayerMask groundLayer)
    {
        jumperRigidbody = rigidbody;
        GroundCollider = groundCollider;
        GroundLayer = groundLayer;
        HasActiveJump = false;
        OverlapRatio = 0.15F;
    }

    public void CatchJumpCommand()
    {
        if(Input.GetButtonDown("Jump")  && HasGroundConnection && !IsGamePaused)
        {
            HasActiveJump = true;
        }
    }

    public void CheckGroundConnection() //Checks if Player has Connection to Ground and sets HasGroundConnection
    {
        HasGroundConnection = Physics2D.OverlapCircle(GroundCollider.position, OverlapRatio, GroundLayer); //Setzt Wert je nachdem ob GroundCollider mit einem der Layer �berlappt
    }

    public void ExecuteJump(float jumpPower)
    {
        jumperRigidbody.AddForce(new Vector2(0, jumpPower));
        HasActiveJump = false;
    }
}
