﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Editor:       Wilkin Niclas
//Erstellt:     22.04.2021
//Bearbeitet:   22.04.2021
//Status:       In Bearbeitung
public class MoveHandler : PauseChecker
{
    private readonly Rigidbody2D rigidbody; 

    public bool LookingRight { get; set; }
    public float Hor { get; private set; }
    public event PlayerDelegate DirectionChanged;

    public MoveHandler(Rigidbody2D moveRigidbody) //Instanzieren des Move Handlers
    {
        rigidbody = moveRigidbody; 
    }

    public void CheckPlayerDirection()
    {
        Hor = Input.GetAxis("Horizontal"); //Abfragen Horizontale Achse

        if ((Hor > 0 && !LookingRight) || (Hor < 0 && LookingRight))
        {
            if (DirectionChanged != null) //Auslösen Direction Changed Event sofern Subscriber vorhanden
            {
                DirectionChanged(this, new EventArgs()); //Alle Subscriber Methoden werden ausgeführt
            }
            
        }
    }
    public void SetVelocity(float movementSpeed)
    {
        rigidbody.velocity = new Vector2(Hor * movementSpeed, rigidbody.velocity.y); //Zuweisen der errechnet Geschwindigkeit zum Rigidbody
    }
}